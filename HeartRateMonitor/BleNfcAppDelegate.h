
#import <Cocoa/Cocoa.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <IOBluetooth/IOBluetooth.h>
#import "NSData+Conversion.h"
#import "NSString+dataFromHex.h"
#import "Token.h"

#define WRITE_WAIT 0.1

#define BLE_SIZE 20
#define KEEPALIVE_INTERVAL 5

#define PORTAL_SERVICE_UUID_SHORT @"1530"

@interface BleNfcAppDelegate : NSObject <NSApplicationDelegate, CBCentralManagerDelegate, CBPeripheralDelegate> {
    NSWindow *window;
    NSWindow *scanSheet;
    NSArrayController *arrayController;

    CBCentralManager *manager;
    CBPeripheral *peripheral;
    CBCharacteristic *write;

    NSMutableArray *heartRateMonitors;

    NSString *manufacturer;

    IBOutlet NSButton *connectButton;
    BOOL autoConnect;

    NSColor *currentColor;

    NSDate *lastWrite;
    NSInteger writeQueue;

    NSTimer *keepAlive;
}

@property (retain) NSMutableArray *heartRateMonitors;
@property (retain) NSMutableArray *tokens;
@property (copy) NSString *manufacturer;
@property (copy) NSString *connected;

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSWindow *scanSheet;
@property (assign) IBOutlet NSArrayController *arrayController;

@property (assign) IBOutlet NSTextField *lastMessage;
@property (assign) IBOutlet NSSlider *trapBrightness;
@property (assign) IBOutlet NSProgressIndicator *progress;
@property (assign) IBOutlet NSCollectionView *collection;

- (IBAction)openScanSheet:(id)sender;
- (IBAction)closeScanSheet:(id)sender;
- (IBAction)cancelScanSheet:(id)sender;
- (IBAction)connectButtonPressed:(id)sender;

- (IBAction)updateTrapBrightness:(id)sender;

- (void)startScan;
- (void)stopScan;
- (BOOL)isLECapableHardware;

@end
