
#import "BleNfcAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@implementation BleNfcAppDelegate

@synthesize window;
@synthesize scanSheet;
@synthesize heartRateMonitors;
@synthesize arrayController;
@synthesize manufacturer;
@synthesize connected;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    autoConnect = TRUE; /* uncomment this line if you want to automatically connect to previosly known peripheral */
    self.heartRateMonitors = [NSMutableArray array];
    self.tokens = [Token emptyTokenArray];

    manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    if (autoConnect) {
        [self startScan];
    }

    writeQueue = 0;
}

- (void)dealloc {
    [self stopScan];
    [peripheral setDelegate:nil];
    [peripheral release];
    [heartRateMonitors release];
    [manager release];
    [super dealloc];
}

/*
 Disconnect peripheral when application terminate
*/
- (void)applicationWillTerminate:(NSNotification *)notification {
    if (peripheral) {
        [manager cancelPeripheralConnection:peripheral];
    }
}

static inline BOOL check_bit(unsigned int x, int bitNum) {
    return x & (1L << bitNum);
}

- (void)parseValue:(NSData *)data {
    const uint8_t *reportData = [data bytes];
    uint8_t command = reportData[0];
    NSMutableArray *status = [NSMutableArray arrayWithCapacity:16];
    int count = 0;
    self.lastMessage.stringValue = [NSString stringWithFormat:@"%c", command];

    switch (command) {
        //Reminder, can't instanciate objects inside case statements
        case 'S': //Status
            count = [self decodeStatus:data eachIncoming:^(NSUInteger idx) {
                //Disable while I use trap in slot
                
                self.progress.indeterminate = true;
                [self setColor:[NSColor redColor]];
                [self queryToken:idx forBlock:0];
                
            }];
            if (count > 0) { //Noisy otherwise
                NSLog(@"Status: %u", count);
            }
            break;
        case 'Q': //Query
            NSLog(@"Query: %@", [status componentsJoinedByString:@" "]);
            [self parseQuery:data];
            break;
        case 'W': //Write
            NSLog(@"Write: %@", [status componentsJoinedByString:@" "]);
            break;
        case 'C': //Color
            NSLog(@"Color: %@", [status componentsJoinedByString:@" "]);
            break;
        case 'A': //Ack (I think)
            NSLog(@"A: %@", data);
            break;
        case 'R': //Run?  restart?
            NSLog(@"R: %@", data);
            break;
        case 'K':
            NSLog(@"K: %@", data);
            break;
        default:
            NSLog(@"Unhandled value format: %c, %@", command, data);
            break;
    }
}

- (IBAction)testK:(id)sender {
    NSButton *K = (NSButton*)sender;
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'K';
    bytes[1] = (uint8)K.intValue;
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];
    
    [self sendCommand:data];
}

- (int)decodeStatus:(NSData *)data eachIncoming:(void (^)(NSUInteger idx))callback {
    unsigned int statusBitVector = 0;
    int count = 0;
    [data getBytes:&statusBitVector range:NSMakeRange(1, 4)];
    //NSLog(@"statusBitVector %02X", statusBitVector);
    for (uint8_t index = 0; index < 16; index++) {
        if (check_bit(statusBitVector, index * 2) && check_bit(statusBitVector, index * 2 + 1)) {
            count++;
            callback(index);
        }
    }
    return count;
}

#define FULL_BRIGHTNESS 255

- (void)parseQuery:(NSData *)data {
    const uint8_t *reportData = [data bytes];
    BOOL success = (reportData[1] > 0x0f);
    uint8_t index = reportData[1] & 0x0f;
    uint8_t block = reportData[2];
    uint8_t startByte = 3;

    if (!success) {
        NSLog(@"[ERROR] %c status %i %@", reportData[0], reportData[1], data);
        return;
    }
    NSLog(@"Q %u %u", index, block);
    NSData *blockData = [data subdataWithRange:NSMakeRange(startByte, BLOCK_SIZE)];

    if (self.tokens[index] == [NSNull null]) {
        self.tokens[index] = [Token new];
    }

    Token *token = (Token *)self.tokens[index];
    [token setData:blockData forBlock:block];

    //To be lightweight, we get minimal data and then figure out what new data we need
    if ([token needsMore]) {
        [self queryToken:index forBlock:block + 1];
    } else {
        [self setColor:[NSColor greenColor]];
        [token dump];
        self.progress.indeterminate = false;
    }
}

- (void)queryToken:(uint8)position forBlock:(uint8)block {
    //Position [0,15]
    uint8 bytes[20] = {0};
    bytes[0] = 'Q';
    bytes[1] = position;
    bytes[2] = block; //block number
    if (block >= BLOCK_COUNT) {
        return;
    }
    NSData *data = [NSData dataWithBytes:bytes length:20];
    [self sendCommand:data];
}

- (void)setColor:(NSColor *)inColor {
    //If the color is white (FFFFFF) the portal goesoff
    NSColor *newColor = [inColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
    CGFloat red = [newColor redComponent];
    CGFloat green = [newColor greenComponent];
    CGFloat blue = [newColor blueComponent];

    //Skip if same color
    if (currentColor && [currentColor redComponent] == red && [currentColor greenComponent] == green && [currentColor blueComponent] == blue) {
        return;
    }

    currentColor = newColor;
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'C';
    bytes[1] = (uint8)(red * 0xFF);
    bytes[2] = (uint8)(green * 0xFF);
    bytes[3] = (uint8)(blue * 0xFF);
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];
    [self sendCommand:data];
}

- (void)ack:(BOOL)state {
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'A';
    bytes[1] = state ? 0x00 : 0x01;
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];

    [self sendCommand:data];
}

- (void)reset {
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'R';
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];
    [self sendCommand:data];
}

- (void)status {
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'S';
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];
    [self sendCommand:data];
}

- (void)updateTrapBrightness:(id)sender {
    [self setTrapLight:self.trapBrightness.intValue];
}

- (void)setTrapLight:(uint8_t)value {
    //0 = set ring color
    //1 = adjustable brightness
    //2 = set ring color
    //3 = full brightness
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'L';
    bytes[1] = 0;
    bytes[2] = 0xFF;
    bytes[3] = 0x00;
    bytes[4] = 0x00;
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];
    [self sendCommand:data];
}

- (IBAction)testJ:(id)sender {
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'J';
    bytes[1] = 0;
    bytes[2] = 0x00;
    bytes[3] = 0x00;
    bytes[4] = 0xFF;
    bytes[5] = 0x00;
    bytes[6] = 0x10;
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];
    
    [self sendCommand:data];
}

- (IBAction)testB:(id)sender {
    uint8 bytes[BLE_SIZE] = {0};
    bytes[0] = 'B';
    //no params
    NSData *data = [NSData dataWithBytes:bytes length:BLE_SIZE];
    
    [self sendCommand:data];
}


- (void)sendCommand:(NSData *)data {
    if (lastWrite && lastWrite.timeIntervalSinceNow * -1 < WRITE_WAIT) {
        writeQueue++;
        double delay = lastWrite.timeIntervalSinceNow * -1 + writeQueue * WRITE_WAIT * 2;
        [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(delayCommand:) userInfo:@{ @"data" : data } repeats:NO];
        return;
    }
    lastWrite = [[NSDate alloc] init];
    NSLog(@"=> %@", data);

    [peripheral writeValue:data forCharacteristic:write type:CBCharacteristicWriteWithResponse];
}

- (void)delayCommand:(NSNotification *)notification {
    //NSLog(@"%s writeQueue %li", __PRETTY_FUNCTION__, writeQueue);
    NSDictionary *userInfo = notification.userInfo;
    [self sendCommand:userInfo[@"data"]];
    writeQueue--;
}

#pragma mark - Scan sheet methods

/*
 Open scan sheet to discover peripherals if it is LE capable hardware
*/
- (IBAction)openScanSheet:(id)sender {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    if ([self isLECapableHardware]) {
        autoConnect = FALSE;
        [arrayController removeObjects:heartRateMonitors];
        [self.window beginSheet:self.scanSheet completionHandler:^(NSModalResponse returnCode) {
            [self stopScan];
            if (returnCode == NSAlertFirstButtonReturn) {
                NSIndexSet *indexes = self.arrayController.selectionIndexes;
                if (indexes.count > 0) {
                    NSUInteger anIndex = indexes.firstIndex;
                    peripheral = self.heartRateMonitors[anIndex];
                    connectButton.title = @"Cancel";
                    [manager connectPeripheral:peripheral options:nil];
                }
            }
        }];
        [self startScan];
    }
}

/*
   Close scan sheet once device is selected
 */
- (IBAction)closeScanSheet:(id)sender {
    [self.window endSheet:self.scanSheet returnCode:NSAlertFirstButtonReturn];
}

/*
   Close scan sheet without choosing any device
 */
- (IBAction)cancelScanSheet:(id)sender {
    [self.window endSheet:self.scanSheet returnCode:NSAlertSecondButtonReturn];
}

#pragma mark - Connect Button

/*
 This method is called when connect button pressed and it takes appropriate actions depending on device connection state
 */
- (IBAction)connectButtonPressed:(id)sender {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    if (peripheral && peripheral.isConnected) {
        /* Disconnect if it's already connected */
        [manager cancelPeripheralConnection:peripheral];
    } else if (peripheral) {
        [connectButton setTitle:@"Connect"];
        [manager cancelPeripheralConnection:peripheral];
        [self openScanSheet:nil];
    } else { /* No outstanding connection, open scan sheet */
        [self openScanSheet:nil];
    }
}

#pragma mark - Start/Stop Scan methods

/*
 Uses CBCentralManager to check whether the current platform/hardware supports Bluetooth LE. An alert is raised if Bluetooth LE is not enabled or is not supported.
 */
- (BOOL)isLECapableHardware {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *state = nil;

    switch (manager.state) {
        case CBCentralManagerStateUnsupported:
            state = @"The platform/hardware doesn't support Bluetooth Low Energy.";
            break;
        case CBCentralManagerStateUnauthorized:
            state = @"The app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBCentralManagerStatePoweredOff:
            state = @"Bluetooth is currently powered off.";
            break;
        case CBCentralManagerStatePoweredOn:
            return TRUE;
        case CBCentralManagerStateUnknown:
        default:
            return FALSE;
    }
    return FALSE;
}

/*
 Request CBCentralManager to scan for heart rate peripherals using service UUID 0x180D
 */
- (void)startScan {
    [manager scanForPeripheralsWithServices:@[ [CBUUID UUIDWithString:@"1530"] ] options:nil];
}

/*
 Request CBCentralManager to stop scanning for heart rate peripherals
 */
- (void)stopScan {
    [manager stopScan];
}

#pragma mark - CBCentralManager delegate methods
/*
 Invoked whenever the central manager's state is updated.
 */
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    [self isLECapableHardware];
}

/*
 Invoked when the central discovers heart rate peripheral while scanning.
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    //NSLog(@"peripheral %@ with advertisementData: %@", aPeripheral, advertisementData);
    NSMutableArray *peripherals = [self mutableArrayValueForKey:@"heartRateMonitors"];
    if (![self.heartRateMonitors containsObject:aPeripheral]) {
        [peripherals addObject:aPeripheral];
    }

    /* Retreive already known devices */
    if (autoConnect) {
        [manager retrievePeripherals:@[ (id)aPeripheral.UUID ]];
    }
}

/*
 Invoked when the central manager retrieves the list of known peripherals.
 Automatically connect to first known peripheral
 */
- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    //NSLog(@"Retrieved peripheral: %lu - %@", [peripherals count], peripherals);

    [self stopScan];

    /* If there are any known devices, automatically connect to it.*/
    if (peripherals.count > 0) {
        peripheral = peripherals[0];
        [peripheral retain];
        connectButton.title = @"Cancel";
        [manager connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey : [NSNumber numberWithBool:YES]}];
    }
}

/*
 Invoked whenever a connection is succesfully created with the peripheral.
 Discover available services on the peripheral
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral {
    //NSLog(@"didConnectPeripheral %@", aPeripheral);
    [aPeripheral setDelegate:self];
    [aPeripheral discoverServices:nil];
    keepAlive = [NSTimer scheduledTimerWithTimeInterval:KEEPALIVE_INTERVAL target:self selector:@selector(status) userInfo:nil repeats:YES];

    self.connected = @"Connected";
    [connectButton setTitle:@"Disconnect"];
}

/*
 Invoked whenever an existing connection with the peripheral is torn down.
 Reset local variables
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error {
    self.connected = @"Not connected";
    [connectButton setTitle:@"Connect"];
    self.manufacturer = @"";
    if (peripheral) {
        [peripheral setDelegate:nil];
        [peripheral release];
        peripheral = nil;
        if (keepAlive) {
            [keepAlive invalidate];
            keepAlive = nil;
        }
    }
}

/*
 Invoked whenever the central manager fails to create a connection with the peripheral.
 */
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error {
    NSLog(@"Fail to connect to peripheral: %@ with error = %@", aPeripheral, [error localizedDescription]);
    [connectButton setTitle:@"Connect"];
    if (peripheral) {
        [peripheral setDelegate:nil];
        [peripheral release];
        peripheral = nil;
    }
}

#pragma mark - CBPeripheral delegate methods
/*
 Invoked upon completion of a -[discoverServices:] request.
 Discover available characteristics on interested services
 */
- (void)peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error {
    //NSLog(@"Peripheral found %@", aPeripheral);
    for (CBService *aService in aPeripheral.services) {
        //NSLog(@"Service found %@", aService);
        [aPeripheral discoverCharacteristics:nil forService:aService];
    }
}

/*
 Invoked upon completion of a -[discoverCharacteristics:forService:] request.
 Perform appropriate operations on interested characteristics
 */
- (void)peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    //CBUUID *updateCharacteristic = [CBUUID UUIDWithString:@"533E1542-3ABE-F33F-CD00-594E8B0A8EA3"];
    //CBUUID *writeCharacteristic = [CBUUID UUIDWithString: @"533E1543-3ABE-F33F-CD00-594E8B0A8EA3"];
    for (CBCharacteristic *aChar in service.characteristics) {
        //NSLog(@"Found characteristic %@ of service %@", aChar.UUID, service.UUID);
        if ((aChar.properties & CBCharacteristicPropertyRead) == CBCharacteristicPropertyRead &&
            (aChar.properties & CBCharacteristicPropertyNotify) == CBCharacteristicPropertyNotify) {
            //NSLog(@"Subscribing to characteristic %@ of service %@", aChar.UUID, service.UUID);
            [peripheral setNotifyValue:YES forCharacteristic:aChar];
        } else if ((aChar.properties & CBCharacteristicPropertyWrite) == CBCharacteristicPropertyWrite) {
            write = aChar;
            //[self ack:YES];
        }
    }
}

- (void)peripheral:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        NSLog(@"%s: error: %@", __PRETTY_FUNCTION__, error);
    }
}

/*
 Invoked upon completion of a -[readValueForCharacteristic:] request or on the reception of a notification/indication.
 */
- (void)peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if ((characteristic.value) || !error) {
        [self parseValue:characteristic.value];
    }
    if (error) {
        NSLog(@"Error: %@", error);
    }
    NSArray *descriptors = characteristic.descriptors;
    if (descriptors.count > 0) {
        NSLog(@"Descriptors: %@", descriptors);
    }

    //bit vector
    CBCharacteristicProperties properties = characteristic.properties;
    if (properties > 0) {
        //NSLog(@"Properties: %0lX", properties);
    }
}

@end
