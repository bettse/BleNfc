//
//  Token.m
//  BleNfc
//
//  Created by Eric Betts on 3/20/15.
//  Copyright (c) 2015 Apple Inc. All rights reserved.
//

#import "Token.h"

@implementation Token

- (Token *)init {
    self = [super init];
    if (self) {
        dataBlocks = [NSMutableArray new];
        ndefSectors = [NSMutableArray new];
    }
    return self;
}

- (BOOL)hasMadv1 {
    BOOL hasMadv1 = NO;
    uint8_t gpb;
    NSData *trailer = [self getTrailerForSector:0];
    [trailer getBytes:&gpb range:NSMakeRange(9, 1)]; //after the keya and acl of the sector trailer
    hasMadv1 = (gpb & 0x80) && (gpb & 0x03) == 1;

    if (hasMadv1) {                                                 //Find the ndef sectors
        for (uint8_t sector = 1; sector < SECTOR_COUNT; sector++) { //Start at sector 1 since 0 had the MAD
            if ([self sectorAid:sector] == NDEF_AID) {
                [ndefSectors addObject:[NSNumber numberWithInteger:sector]];
            } else {
                //NSLog(@"non NDEF sector %u", sector);
            }
        }
    } else {
        NSLog(@"No MAD found, trailer: %@", trailer);
    }
    return hasMadv1 && ndefSectors.count > 0;
}

- (BOOL)needsMore {
    return (dataBlocks.count < BLOCK_COUNT);
}

- (NSUInteger)blocksNeeded {
    //4 blocks per sector, +1 for sector0 since it can't be an ndefSector
    return (ndefSectors.count + 1) * 4;
}

#define TLV_NULL 0x00 // These blocks should be ignored
#define TLV_NDEF 0x03 //Block contains an NDEF message
#define TLV_PROP 0xFD //Block contains proprietary information
#define TLV_TERM 0xFE //Last TLV block in the data area

- (void)parseTag {
    if ([self needsMore]) {
        return;
    }
    NSMutableData *ndefSectorData = [NSMutableData data];

    for (NSNumber *sector in ndefSectors) {
        [ndefSectorData appendData:[self getDataForSector:[sector integerValue]]];
    }

    [self parseTLV:ndefSectorData];
}

- (void)parseTLV:(NSData *)data {
    uint8_t *bytes = (uint8_t *)data.bytes;
    uint8_t offset = 0;
    uint8_t type = 0;
    uint16_t length = 0;

    do {
        type = bytes[offset++];
        length = bytes[offset++];
        if (length == 0xff) {
            memcpy(&length, bytes + offset, sizeof(uint16_t));
        }

        switch (type) {
            case TLV_NULL:
                offset++;
                break;
            case TLV_NDEF:
                [self parse_ndef_message:bytes + offset];
                offset += length;
                break;
            case TLV_TERM:
                offset = data.length;
                break;
            default:
                break;
        }
    } while (type != TLV_TERM && offset < data.length);
}

- (uint16_t)sectorAid:(NSUInteger)sector {
    uint16_t sector_aid;
    uint8_t block = 1 + sector / 8;                     //+1 since block0 is manufacturer data
    uint8_t offset = sizeof(sector_aid) * (sector % 8); // *2 since the value is 2 bytes
    [dataBlocks[block] getBytes:&sector_aid range:NSMakeRange(offset, sizeof(sector_aid))];
    //NSLog(@"sector %lu <%u,%u> = %x", sector, block, offset, sector_aid);
    return sector_aid;
}

- (void)setData:(NSData *)data forBlock:(NSUInteger)block {
    //NSLog(@"%s %lu %@", __PRETTY_FUNCTION__, block, data);
    [dataBlocks insertObject:data atIndex:block];
}

- (NSData *)getTrailerForSector:(NSUInteger)sector {
    NSUInteger trailer = sector * 4 + 3;
    return dataBlocks[trailer];
}

- (NSData *)getDataForSector:(NSUInteger)sector {
    NSUInteger start = sector * 4;
    NSMutableData *sectorData = [[NSData data] mutableCopy];
    [sectorData appendData:dataBlocks[start + 0]];
    [sectorData appendData:dataBlocks[start + 1]];
    [sectorData appendData:dataBlocks[start + 2]];
    return sectorData;
}

+ (NSMutableArray *)emptyTokenArray {
    NSMutableArray *empty = [NSMutableArray arrayWithCapacity:MAX_TOKENS];
    for (int i = 0; i < MAX_TOKENS; i++) {
        [empty insertObject:[NSNull new] atIndex:i];
    }
    return empty;
}

/**
 * Convert TNF bits to Type (used for debug only). This is specified in
 * section 3.2.6 "TNF (Type Name Format)" of "NFC Data Exchange Format
 * (NDEF) Technical Specification".
 *
 * @param IN  b  the value of the last 3 bits of the NDEF record header
 * @return       the human readable type of the NDEF record
 */
- (char *)type_name_format:(int)b {
    switch (b) {
        case 0x00:
            return "Empty";
        case 0x01:
            return "NFC Forum well-known type [NFC RTD]";
        case 0x02:
            return "Media-type as defined in RFC 2046 [RFC 2046]";
        case 0x03:
            return "Absolute URI as defined in RFC 3986 [RFC 3986]";
        case 0x04:
            return "NFC Forum external type [NFC RTD]";
        case 0x05:
            return "Unknown";
        case 0x06:
            return "Unchanged";
        case 0x07:
            return "Reserved";
        default:
            return "Invalid TNF byte!";
    }
}

/**
 * Convert type of URI to the actual URI prefix to be used in conjunction
 * with the URI stored in the NDEF record itself. This is specified in section
 * 3.2.2 "URI Identifier Code" of "URI Record Type Definition Technical
 * Specification".
 *
 * @param IN  b  the code of the URI to convert to the actual prefix
 * @return       the URI prefix
 */
- (char *)uri_identifier_code:(int)b {
    /*
     * Section 3.2.2 "URI Identifier Code" of "URI Record Type Definition
     * Technical Specification"
     */
    switch (b) {
        case 0x00:
            return "";
        case 0x01:
            return "http://www.";
        case 0x02:
            return "https://www.";
        case 0x03:
            return "http://";
        case 0x04:
            return "https://";
        case 0x05:
            return "tel:";
        case 0x06:
            return "mailto:";
        case 0x07:
            return "ftp://anonymous:anonymous@";
        case 0x08:
            return "ftp://ftp.";
        case 0x09:
            return "ftps://";
        case 0x0A:
            return "sftp://";
        case 0x0B:
            return "smb://";
        case 0x0C:
            return "nfs://";
        case 0x0D:
            return "ftp://";
        case 0x0E:
            return "dav://";
        case 0x0F:
            return "news:";
        case 0x10:
            return "telnet://";
        case 0x11:
            return "imap:";
        case 0x12:
            return "rtsp://";
        case 0x13:
            return "urn:";
        case 0x14:
            return "pop:";
        case 0x15:
            return "sip:";
        case 0x16:
            return "sips:";
        case 0x17:
            return "tftp:";
        case 0x18:
            return "btspp://";
        case 0x19:
            return "btl2cap://";
        case 0x1A:
            return "btgoep://";
        case 0x1B:
            return "tcpobex://";
        case 0x1C:
            return "irdaobex://";
        case 0x1D:
            return "file://";
        case 0x1E:
            return "urn:epc:id:";
        case 0x1F:
            return "urn:epc:tag:";
        case 0x20:
            return "urn:epc:pat:";
        case 0x21:
            return "urn:epc:raw:";
        case 0x22:
            return "urn:epc:";
        case 0x23:
            return "urn:nfc:";
        default:
            return "RFU";
    }
}

/**
 * Concatenates the prefix with the contents of the NDEF URI record.
 *
 * @param IN payload      The NDEF URI payload
 * @param IN payload_len  The length of the NDEF URI payload
 * @return                The full reconstructed URI
 */
- (char *)ndef_parse_uri:(uint8_t *)payload withLength:(int)payload_len {
    char *prefix = [self uri_identifier_code:payload[0]];
    size_t prefix_len = strlen(prefix);
    char *url = malloc(prefix_len + payload_len);
    memcpy(url, prefix, prefix_len);
    memcpy(url + prefix_len, payload + 1, payload_len - 1);
    url[prefix_len + payload_len - 1] = 0x00;
    return url;
}

- (BOOL)isSet:(uint8_t)data andMask:(uint8_t)mask {
    return (data & mask) == mask;
}

/**
 * Parse the actual NDEF message and call specific handlers for dealing with
 * a particular type of NDEF message.
 *
 * @param IN ndef_msg  The NDEF message
 * @return             whether or not the parsing succeeds
 */
#define TNF_MASK 0x07
- (BOOL)parse_ndef_message:(uint8_t *)ndef_msg {
    /* analyze the header field */
    int offset = 0;
    bool me;
    do {
        bool mb = [self isSet:ndef_msg[offset] andMask:0x80]; /* Message Begin */
        me = [self isSet:ndef_msg[offset] andMask:0x40];      /* Message End */
        bool cf = [self isSet:ndef_msg[offset] andMask:0x20]; /* Chunk Flag */
        bool sr = [self isSet:ndef_msg[offset] andMask:0x10]; /* Short Record */
        bool il = [self isSet:ndef_msg[offset] andMask:0x08]; /* ID Length Present */
        char *typeNameFormat = [self type_name_format:ndef_msg[offset] & TNF_MASK];
        offset++;

        if (DEBUG) {
            NSLog(@"MB=%d ME=%d CF=%d SR=%d IL=%d TNF=%s", mb, me, cf, sr, il, typeNameFormat);
        }
        if (cf) {
            NSLog(@"chunk flag not supported yet");
            return false;
        }

        int typeLength = ndef_msg[offset];
        offset++;

        uint32_t payloadLength = 0;
        if (sr) {
            payloadLength = ndef_msg[offset];
            offset++;
        } else { //4 byte unsigned int representing size
            //FIXME payloadLength = Utils.byteArrayToInt(data, offset);
            memcpy(&payloadLength, ndef_msg + offset, 4);
            offset += 4;
        }

        int idLength = 0;
        if (il) {
            idLength = ndef_msg[offset];
            offset++;
        }
        char *type = malloc(typeLength);
        memcpy(type, ndef_msg + offset, typeLength);

        offset += typeLength;

        uint8_t *id;
        if (il) {
            id = malloc(idLength);
            memcpy(id, ndef_msg + offset, idLength);
            offset += idLength;
        }
        if (DEBUG) {
            NSLog(@"typeLength=%d payloadLength=%d idLength=%d type=%s", typeLength, payloadLength, idLength, type);
        }
        uint8_t *payload = malloc(payloadLength);

        memcpy(payload, ndef_msg + offset, payloadLength);
        offset += payloadLength;

        if (strncmp("U", type, typeLength) == 0) {
            /* handle URI case */
            char *uri = [self ndef_parse_uri:payload withLength:payloadLength];
            NSString *path = [NSString stringWithCString:uri encoding:NSUTF8StringEncoding];
            NSLog(@"URI = %s / path = %@", uri, path);
            if (payload[0] == 0x1D) {
                if ([path hasSuffix:@".app"]) {
                    BOOL result = [[NSWorkspace sharedWorkspace] launchApplication:path];
                    NSLog(@"Launch app %@", result ? @"Successful" : @"Failed");
                } else { //A plain file
                    BOOL result = [[NSWorkspace sharedWorkspace] openFile:path];
                    NSLog(@"Open file %@", result ? @"Successful" : @"Failed");
                }
            } else { //non-file url
                BOOL result = [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:path]];
                NSLog(@"Open url %@", result ? @"Successful" : @"Failed");
            }
        } else if (strncmp("T", type, typeLength) == 0) {
            NSLog(@"Text");
        } else if (strncmp("Sp", type, typeLength) == 0) {
            NSLog(@"SmartPoster");
        } else {
            NSLog(@"unsupported (non-url) NDEF record type [%c]", type[0]);
            return false;
        }
    } while (!me); /* as long as this is not the last record */

    return true;
}

-(void)dump{
    for (NSData *block in dataBlocks) {
        NSLog(@"%@", block);
    }
}

@end
