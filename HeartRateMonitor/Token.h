//
//  Token.h
//  BleNfc
//
//  Created by Eric Betts on 3/20/15.
//  Copyright (c) 2015 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAX_TOKENS 16 //A 4 byte int is used to store token state, and 2 bits are used per token.  4 * 8 / 2 = 16
#define SECTOR_COUNT 16
#define SECTOR_SIZE 4
#define BLOCK_SIZE 16
#define BLOCK_COUNT (4 * SECTOR_COUNT)
#define NFC_FORUM_FCC 0xe1
#define NDEF_APP_CODE 0x03
#define NDEF_AID 0xe103

@interface Token : NSObject { //I may need to further subclass into tnp3xx and non
    NSMutableArray *dataBlocks;
    NSMutableArray *ndefSectors;
    uint8_t ndefSectorCount;
}

+ (NSMutableArray *)emptyTokenArray;

- (BOOL)needsMore;
- (void)parseTag;
- (void)dump;
- (void)setData:(NSData *)data forBlock:(NSUInteger)block;

@end
