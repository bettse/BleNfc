#Bluetooth Low Energy NFC reader

53 6b 79 6c 61 6e 64 65 72 73 20 50 6f 72 74 61 6c

### Notes

ServiceUUID = 0x1530

sample characteristic.value: 53000000 001f00aa 91021900 00000000 00000000

###Decoding status message

Bytes 1-5: pairs represent token and state (coming/going)
  higher value bit: token state changing
  lower value bit: token present

00 = no token
01 = token present
10 = token leaving (present, changing state to not present)
11 = token entering (not present, changing state to present)

    2014-11-29 20:27:33.305 HeartRateMonitor[66383:27087182] Sending query for block 0 of token 33 (<51210000 00000000 00000000 00000000 00000000>)
    2014-11-29 20:27:33.305 HeartRateMonitor[66383:27087182] Status update:       ↓
    2014-11-29 20:27:33.395 HeartRateMonitor[66383:27087182] Sending query for block 0 of token 32 (<51200000 00000000 00000000 00000000 00000000>)
    2014-11-29 20:27:33.395 HeartRateMonitor[66383:27087182] Query: Error, trying again <51010000 00000000 00000000 00000000 00000000>
    2014-11-29 20:27:33.515 HeartRateMonitor[66383:27087182] Query: Success 0x10 <511000da ae7e1b11 81010fc4 39000000 00001200>

### Scanned tokens using nfc reader
SENS_RES is has been renamed ATQA; SEL_RES has bee renamed SAK

Bigger token

    2014-11-29 13:38:38 UTC-8b0a8ea3[Device]  count: 1, SENS_RES: 10e SEL_RES: 01 tag_len: 4, tag_id:daae7e1b
    2014-11-29 13:38:41 UTC-8b0a8ea3[Device]  count: 1, SENS_RES: 10e SEL_RES: 01 tag_len: 4, tag_id:daae7e1b
    2014-11-29 13:38:44 UTC-8b0a8ea3[Device]  count: 1, SENS_RES: 10e SEL_RES: 01 tag_len: 4, tag_id:daae7e1b
Smaller Token

    2014-11-29 13:38:50 UTC-8b0a8ea3[Device]  count: 1, SENS_RES: 10e SEL_RES: 01 tag_len: 4, tag_id:af86e4be
    2014-11-29 13:38:53 UTC-8b0a8ea3[Device]  count: 1, SENS_RES: 10e SEL_RES: 01 tag_len: 4, tag_id:af86e4be



51822 qfab80 1406as
